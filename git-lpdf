#!/usr/bin/env bash

## ======================
## git-lpdf and git-ldiff
## ======================
##
## A bash script for producing pdf files from a git repository that tracks latex
## documents. There are two modes:
## - lpdf:  produces a latex file for the given commit in the repository
## - ldiff: uses latexdiff to create a pdf file that highlights the differences between commits
## This script is partially motivated by the script git-latexdiff_ and my
## attempts to get it to work the way that I wanted. The date and commit information is printed
## as a banner on the PDF files. This script should be used from inside git.
##
## The main idea of the script is to provide an easy way to produce a PDF file
## from a git repository that is clearly annotated with the commit data. For
## example.
##
## .. code-block:: bash
##
##    > git lpdf <commit>
##
## will produce a PDF file for the commit <commit> of the latex file in the current
## repository. Using latexdiff_, the command
##
## .. code-block:: bash
##
##    > git ldiff <commit>
##
## produces a PDF file that highlights the differences between the commit
## <commit> and the current ## working copy of the latex file in the current
## repository. Similarly,
##
## .. code-block:: bash
##
##    > git ldiff <commit1> <commit2>
##
## produces a PDf file showing he differences between two commits.
##
## In all cases the commit information is printed on all pages of the PDF.
##
## Andrew Mathas June 2014
##
## Installation
## ------------
##
## Clone the git repository, or download the shell script, and then type either:
##
## .. code-block:: bash
##
##   ./git-ldiff --install [directory]   # directory defaults to $HOME/bin
##
## or
##
## .. code-block:: bash
##
##   ./git-ldiff --linkinstall [directory]   # directory defaults to $HOME/bin
##
## The first version copies the script to <directory>/git-lpdf and creates a
## link from <directory>/git-ldiff to <directory>/git-lpdf  The second variation
## creates two links to the script in its current location, which is useful if
## you have cloned the git repository for git-lpdf.
##
## The script makes use of the background_ package and latexdiff_. Both of these
## packages are available from ctan_ and they are automatically installed with TeXLive.

# defaults
cleaning="yes"
banner=""
debugging="no"
keep_latex="no"
latex="pdflatex -interaction=nonstopmode"
latex_opt=""
open="open"   # sensible default only on macosx
old_commit=""
new_commit=""
header=""
tex_file=""
latexdiff_type="CULINECHBAR"
ldiff_options=" --flatten --ignore-warnings --packages=amsmath,hyperref --subtype=COLOR "
if [[ -r $HOME/.latexdiffrc ]]; then
  ldiff_options="${ldiff_options} -c $HOME/.latexdiffrc"
fi

function exit_with_error()
{
  echo "$prog error: $*"
  exit 1
}

# defaults
case $(basename $0) in
  *diff*) prog="ldiff" ;;
  *pdf*)  prog="lpdf" ;;
  *)      exit_with_error "usage: invalid program name" ;;
esac

top_dir=$(PWD)       # save the current working directory
tmp_dir="/tmp/$prog" # default temporary directory

# program specific defaults
case $prog in
  ldiff)
    old_commit_default="HEAD"
    new_commit_default="--"
    ;;
  lpdf)
    old_commit_default="--"
    new_commit_default=""  # will not be used
    ;;
esac

ldiff_usage="$(cat << END_USAGE
Usage: git ldiff [--main file] [--latex latex executable] [OLD] [NEW]

To use this mode you need to have latexdiff_ installed.

The following command line options are supported:

  -b|--banner <banner>  : set the banner printed in the left-hand margin
  -d|--debug            : debug mode
  -k|--keep             : keep the changes LaTeXZ file
  -l|--latex            : set latex executable
     --latex-opt        : commandline options passed to latex
  -m|--main   <texfile> : set the name of the main latex file
  -s|--safe             : more robust diff of mathematics by latexdiff
  -S|--verysafe         : very robust diff of mathematics by latexdiff
  -t|--tmp              : set temporary directory used during compilation
  -T|--latexdiff-type   : specify type of diff used by latexdiff

By default the files in the HEAD of the git repository are compared with
the files in current working directory. Ostensibly, OLD and NEW are git shas
in the current repository, however, we also allow them to be --, for the files
in the current working directory, or another directory.

Examples:

.. code-block:: bash

  > git ldiff   # compare most recent commit with current (uncommited) version
  > git ldiff --main myfile.tex  # compare most recent commit for myfile  with current version
  > git ldiff <commit> # compare commit with current verion
  > git ldiff <dirame> [commit] # compare version in directory <dirname> with specified commit

There are also --safe and --verysafe options that are sometimes more
successful in getting latexdiff to work.
END_USAGE
)"

lpdf_usage="$(cat << END_USAGE
Usage: git lpdf [--main file] [--latex latex executable] [commit]

Creates a PDF file for the main latex file in the repository with commit
information printed as a banner down the left hand margin on each page.

The following command line options are supported:

  -b|--banner <banner>  : set the banner printed in the left-hand margin
  -d|--debug            : debug mode
  -l|--latex            : set latex executable
     --latex-opt        : commandline options passed to latex
  -m|--main   <texfile> : set the name of the main latex file

Examples:

.. code-block:: bash

  > git lpdf           # produces time-stamped "Latest version" of main latex file
  > git lpdf b675cdf   # produces pdf file for main latex file as of commit b675cdf
  > git lpdf <directory> [commit] # compare file in given directory with commit
  > git lpdf --main myfile.tex # produces pdf file for my file as of commit b675cdf

By default the script uses pdflatex. This can be changed using the --latex option:

.. code-block:: bash

  > git lpdf --latex   # produces time-stamped "Latest version" of main latex file

END_USAGE
)"

# print usage messages, stripping out code-blocks and any repeated (blank) lines
function usage()
{
  case $prog in
    ldiff) echo "$ldiff_usage" | grep -v code-block | uniq;;
    lpdf)  echo "$lpdf_usage"  | grep -v code-block | uniq;;
  esac
}

# write the README file
function generate_readme()
{
  readme="$(dirname $0)/readme.rst"
  egrep "^##\s+" $0 | sed -E 's/^##[[:space:]]{0,1}//' > $readme  # macosx specific
  echo -e "\nUsage for lpdf script\n---------------------" >> $readme
  echo -e "$lpdf_usage" >> $readme
  echo -e "\nUsage for ldiff script\n----------------------" >> $readme
  echo -e "$ldiff_usage" >> $readme
  cat <<End_of_Links >> $readme

To do
-----
 - better handling of latexdiff options
 - clean up the argument parsing
 - improve documentation

Licence
-------
GNU General Public License, Version 3, 29 June 2007

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL_) as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

Automatically generated $(date).

.. References
.. ..........
.. _background: http://www.ctan.org/pkg/background
.. _ctan: http://www.ctan.org/
.. _git-latexdiff: https://gitorious.org/git-latexdiff
.. _latexdiff: http://www.ctan.org/pkg/latexdiff
.. _GPL: http://www.gnu.org/licenses/gpl.html
End_of_Links
}

# extract_git_files sha tmp
function extract_git_files
{
  sha="$1"   # sha identifying a commit -- or a directory
  # subdirectory of  $tmp_dir to put the files in
  if [[ $# -eq 2 ]]; then
    tmp="$tmp_dir/$2"
  else
    tmp="$tmp_dir"
  fi
  mkdir -p $tmp
  if [[ "$sha" = "--" ]]; then
    # $target_dir to $top_dir
    for f in $tracked_files; do
      /bin/cp -f "$top_dir/$f" "$tmp/"
    done
  elif [[ -d "$sha" ]]; then
    # $target_dir to $top_dir
    for f in $tracked_files; do
        /bin/cp -f $sha/"$f" "$tmp/"
    done
  else
    # extract files git archive into $tmp
    cd "$git_dir" || exit_with_error "unable to cd to git directory $git_dir"
    if [[ $# -eq 2 ]]; then
      mkdir -p $tmp || exit_with_error "unable to create directory $tmp"
    fi
    git archive --format=tar "$sha" | tar -xC $tmp -f - || exit_with_error "unable to extract files from $sha"
  fi
}

# add a banner down the left-hand margin on each page of the tex file
# usage: add_banner_to_TeX_file<filename> <banner>
function add_banner_to_TeX_file()
{
  if [[ -z $banner ]] ; then
    content=$2
  else
    content=$banner
  fi
  banner="usepackage{background}\\\\backgroundsetup{position={-1,1},anchor=left,angle=90,scale=1.2,pages=all,opacity=1,contents={$content}}\\\\"
  sed -i.bak  "s@begin{document}@${banner}&@" "$1"
}

# latex TeX file as many times as necessary
# usage: latex_file <dir> <file>
function latex_file()
{
  cd "$1"
  file="${2%.*}"    # remove the extension from the filename

  # add $top_dir to TEXINPUTS
  if [[ -z "$TEXINPUTS" ]] ; then
    TEXINPUTS=".:${top_dir}/:"
  else
    TEXINPUTS="${TEXINPUTS}${top_dir}/:"
  fi

  $latex "$latex_opt" $file

  case $latex in    # test for the correct extension (pdf or dvi or ?)
    *pdf*) ext="pdf" ;;
    *)     ext="dvi" ;;
  esac

  if [[ -z $file.$ext || ! -r $file.$ext ]] ; then
    # To check for errors, rather than looking at the exit code from latex we
    # check to see if a PDF/dvi files has been created because quite often there
    # are errors in the latex file but we still get a reasonable PDF file.
    # After the first pass we assume that latex-ing is OK
    exit_with_error "unable to latex $file"
  fi

  # if there is an aux file assume we need to update cross-references
  if [[ -s $file.aux ]]; then
    # run bibtex, and re-latex, if there is a \bibdata command in the aux file
    needs_bibtex=$(grep '\\bibdata' $file.aux)
    if [[ -n $needs_bibtex ]]; then
      bibtex $file  # generate bibliography
      $latex $file  # need an extra run of latex
    fi
    $latex $file    # to update cross-references
  fi
}

function ldiff()
{
  # if we have the same git sha1's then there's nothing to do
  [[ "$old_commit" = "$new_commit" ]] && exit_with_error "You are comparing same the sha1's!"

  # extract the archived files
  extract_git_files $old_commit old  # copy to /tmp/old
  extract_git_files $new_commit new  # copy to /tmp/new

  # now create the latexdiff file
  cd $tmp_dir
  [[ -r "old/$tex_file" ]] || exit_with_error "old/$tex_file does not exist"
  [[ -r "new/$tex_file" ]] || exit_with_error "new/$tex_file does not exist"
  latexdiff $ldiff_options --type=$latexdiff_type "old/$tex_file" "new/$tex_file" | sed 's@@@g' > "$tex_file"

  add_banner_to_TeX_file "$tex_file" "$header"

  # change the blue for added text for ForestGreen
  sed -E -i.bak 's@\\color{blue}(.*%DIF PREAMBLE)@\\color{ForestGreen}\1@' $tex_file

  # latex the changes file
  latex_file $tmp_dir "$tex_file"

  # finally, if we are cleaning up then move the pdf file to $top_dir and
  # delete all of the files in $tmp_dir
  if [[ -r "$pdf_file" && "$cleaning" = "yes" ]]; then
    pdf_changes="$top_dir/${pdf_file/.pdf/-changes.pdf}"
    /bin/mv "$tmp_dir/$pdf_file" "$pdf_changes"
    if [[ $keep_latex = "yes" ]]; then
        /bin/mv "$tmp_dir/$tex_file" "$top_dir/${tex_file/.tex/-changes.tex}"
    fi
    $open "$pdf_changes"
    /bin/rm -Rf $tmp_dir
  else
    [[ -r "$pdf_file" ]] && $open $pdf_file
  fi
}

function lpdf()
{
  # extract the archived files
  extract_git_files $old_commit

  # determine banner to specify the commit on each page of the $tex_file
  if [[ "$old_commit" = "--" ]]; then
    commit_message=$(date "+Latest version: %a %d %b %Y")
    old_comment="Latest"
  else
    commit_message=$(git log --pretty='format:%ad, %s, %h' --date=local -n 1 $old_commit)
  fi

  # now create the latexdiff file
  cd $tmp_dir
  [[ -r $tex_file ]] || exit_with_error "$tex_file does not exist"
  add_banner_to_TeX_file "$tex_file" "$commit_message"

  latex_file $tmp_dir "$tex_file"

  # finally, if we are cleaning up then move the pdf file to $top_dir and
  # delete all of the files in $tmp_dir
  if [[ ! -r $pdf_file ]]; then
    echo "No pdf file produced"
  elif [[ $cleaning = "yes" ]]; then
    /bin/mv "$tmp_dir/$pdf_file" "$top_dir/${old_commit}-$pdf_file"
    $open -n "$top_dir/${old_commit}-$pdf_file"
    /bin/rm -Rf $tmp_dir
  else
    $open "$tmp_dir/$pdf_file"
  fi
}

# first determine if we are in a git repository and, if so, where the repository is
git_dir="$(git rev-parse --show-toplevel)/.git"
[[ ! -d "$git_dir" ]] && exit_with_error "not in a git repository?"

# parse command line arguments
while [[ $# -ne 0 ]]; do
  case $1 in
    -h|--help) usage && exit ;;
    --banner|-b)
      shift
      [[ $# -eq 0 ]] && exit_with_error "--banner requires an argument"
      banner="$1" && shift
      ;;
    --main|-m)
      shift
      [[ $# -eq 0 ]] && exit_with_error "--main requires an argument"
      tex_file="$1" && shift
      ;;
    --latex|-l)
      shift
      [[ $# -eq 0 ]] && exit_with_error "--latex requires an argument"
      latex="$1" && shift
      text ! -x $latex && exit_with_error "latex executable $latex not executable"
      ;;
    --latex-opt)
      shift
      [[ $# -eq 0 ]] && exit_with_error "--latex requires an argument"
      latex_opt="$1" && shift
      ;;
    --tmp_dir|-t)
      shift
      [[ $# -eq 0 ]] && exit_with_error "--tmp requires an argument"
      tmp_dir="$1" && shift
      [[ "$tmp_dir" = "job" ]] && tmp_dir="/tmp/latexdiff-$$"
      ;;
    --open|-o)
      shift
      [[ $# -eq 0 ]] && exit_with_error "--open requires an argument"
      open="$1" && shift
      ;;
    --nocleaning|-n)
      cleaning="no"
      shift
      ;;
    --debug*|-d)
      set -vx  # turn on inbuilt debugging
      debugging="yes"
      cleaning="no"
      shift
      ;;
    --keep|-k)
      keep_latex="yes"
      shift
      ;;
    --latexdiff-type|-T)
      shift
      latexdiff_type="$1" && shift
      ;;
    --safe|-s)
      # add extra options to ldiff_options so that latexdiff has a better chance of working
      ldiff_options="$ldiff_options --math-markup=whole"
      shift
      ;;
    --verysafe|-S)
      # add extra options to ldiff_options so that latexdiff has an even better chance of working
      ldiff_options="$ldiff_options --math-markup=off"
      shift
      ;;
    --install)
      if [[ $# -gt 1 ]]; then
        shift
        location=$1
      else
        location=$HOME/bin
        [[ ! -d $location ]] && exit_with_error "--install <dir>: dir must be a directory"
      fi
      cp $0 $location/git-lpdf
      ln -s $location/git-lpdf $location/git-ldiff
      exit 0
      ;;
   --linkinstall)
      if [[ $# -gt 1 ]]; then
        shift
        location=$1
      else
        location=$HOME/bin
        [[ ! -d $location ]] && exit_with_error "--linkinstall <dir>: dir must be a directory"
      fi
      case $(dirname $0) in
        \.) me="$(pwd)/$(basename $0)" ;;
         *) me=$0 ;;
      esac
      echo "Linking to $me"
      [[ $0 != $location/git-lpdf ]] && ln -s $me $location/git-lpdf
      ln -s $me $location/git-ldiff
      exit 0
      ;;
    --readme)
      generate_readme
      exit 0
      ;;
    *)
      if [[ -z $old_commit ]]; then
        old_commit="$1"
      elif [[ -z $new_commit ]] ; then
        new_commit="$1"
      else
        usage && exit 1
      fi
      shift
      ;;
  esac
done

[[ -z $old_commit ]] && old_commit=$old_commit_default
[[ -z $new_commit ]] && new_commit=$new_commit_default

# by default the changes header lists the two diffs
if [[ "$changes" ]] ; then
  changes="\\\\LaTeX diff:  $old_commit \\\\quad$\\\\leftrightarrow$\\\\quad $new_commit"
fi

# If the tex_file has not been specified we try to guess it and exit if the
# repository contains more than one tex file with a \documentclass commend..
if [[ -z "$tex_file" ]] ; then
  tex_file="$(git ls-files | grep .tex)"
  [[ ! -r "$tex_file" ]] && exit_with_error "Tex file <$tex_file> is ambiguous. Specify filename using --main (found $tex_file)"
elif [[ ! -r "$tex_file" ]]; then
  exit_with_error "Unable to read file $tex_file"
fi
pdf_file=${tex_file/.tex/.pdf}

# create a string of tracked files in the git repository
if [[ $(git ls-files | wc -l) -gt 1 ]]; then
  tracked_files="$(git ls-files|xargs)"
else
  tracked_files="$(git ls-files)"
fi

# if using the default tmp directory then delete it otherwise try to create it.
[[ $tmp_dir = "/tmp/$prog" ]] && /bin/rm -Rf /tmp/$prog
if [[ ! -d "$tmp_dir" ]] ; then
  mkdir -p $tmp_dir || exit_with_error "unable to create temporary directory $tmp_dir"
fi

# print debugging information if requested
if [[ $debugging = "yes" ]]; then
  cat << debugging
$prog should be finishing:
  top_dir:    $top_dir
  tmp_dir:    $tmp_dir
  tex_file:   $tex_file
  pdf_file:   $pdf_file
  cleaning:   $cleaning
  old commit: $old_commit
  new commit: $new_commit
debugging
fi

# fork to comparing two commits with latexdiff or pdf-ing one commit
case $prog in
  ldiff) ldiff
    ;;
  lpdf)  lpdf
    ;;
esac
